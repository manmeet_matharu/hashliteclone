console.log('Server-side code running');

const express = require('express');
const MongoClient = require('mongodb').MongoClient;

const app = express();

// serve files from the public directory
app.use(express.static('public'));

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// connect to the db and start the express server
let db;


// ***Replace the URL below with the URL for your database***
// const url = 'mongodb://localhost:27017/hashlite';
const url = 'mongodb://foxmulaUser:Pass#123@ds121382.mlab.com:21382/mylabdb';
// E.g. for option 2) above this will be:
// const url =  'mongodb://localhost:21017/databaseName';

MongoClient.connect(url, (err, database) => {
  if (err) {
    return console.log(err);
  }
  db = database;
  // start the express web server listening on 8080
  app.listen(8080, () => {
    console.log('listening on 8080');
  });
});

// serve the homepage
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Term and Condition page route.
app.get('/terms', (req, res) => {
  res.sendFile(__dirname + '/public/termsandcondition.html');
});
app.get('/privacy-policy', (req, res) => {
  res.sendFile(__dirname + '/public/privacy.html');
});

app.get('/sign-up', (req, res) => {
  res.sendFile(__dirname + '/public/sign-up.html');
});

app.get('/sign-in', (req, res) => {
  res.sendFile(__dirname + '/public/sign-in.html');
});
app.get('/support', (req, res) => {
  res.sendFile(__dirname + '/public/support.html');
});

app.post('/register', (req, res) => {
  console.log(req.body);
  var userDetail = req.body;

  db.collection('UserDetails').find(
    {
      userEmail: userDetail.userEmail
    }).toArray((err, result) => {
      if (err) {
        res.sendStatus(err);
      }
      else {
        if (result.length == 0) {
          db.collection('UserDetails').save(userDetail, (err, result) => {
            if (err) {
              return console.log(err);
            }
            console.log('click added to db');
            res.status(200).send(result);
          });
        }
        else {
          res.status(201).send(result);
        }
      }
    });
});


// get the click data from the database
app.post('/login', (req, res) => {
  console.log(req.body);
  var userDetail = req.body;

  db.collection('UserDetails').find(
    {
      userPassword: userDetail.userPassword,
      userEmail: userDetail.userEmail
    }).toArray((err, result) => {
      if (err) {
        res.send(err);
      }
      else {
        res.send(result);
      }
    });
});
